package middleware_test

import (
	"context"
	"net/http"
	"net/url"
	"testing"
	"time"

	"codeberg.org/gruf/go-byteutil"
	"codeberg.org/gruf/go-middleware"
)

func TestRequestID(t *testing.T) {
	t.Skip("todo")
}

func TestLogging(t *testing.T) {
	t.Skip("todo")
}

func TestRecovery(t *testing.T) {
	t.Skip("todo")
}

func TestTimeout(t *testing.T) {
	const defaultTimeout = time.Second * 5

	m := middleware.New(nil)

	// Wrap handler with middleware timeout handler
	handler := m.Timeout(http.HandlerFunc(func(rw http.ResponseWriter, r *http.Request) {
		// Look for expected timeout
		timeout := r.Context().Value("timeout").(time.Duration)
		if timeout < 1 {
			timeout = defaultTimeout
		}

		// Ensure that request times out
		time.Sleep(timeout + time.Second)
		select {
		case <-r.Context().Done():
		default:
			t.Fatal("request did not timeout")
		}

		// ResponseWriter should be timeout
		trw, ok := rw.(*middleware.TimeoutResponseWriter)
		if !ok {
			t.Fatal("responsewriter was not timeout type")
		}

		// Ensure further writes send errors
		_, err := trw.Write([]byte{})
		if err != http.ErrHandlerTimeout {
			t.Fatalf("responsewriter did not timeout: %v", err)
		}
	}), defaultTimeout)

	for _, dur := range []time.Duration{
		0, time.Second, time.Second * 5, time.Second * 10,
	} {
		// Craft request objs
		r, rw := craft(dur)

		// Pass to timeout handler
		handler.ServeHTTP(rw, r)

		// Check for expected response
		if rw.code != http.StatusRequestTimeout ||
			string(rw.buf.B) != "Request Timed Out\n" {
			t.Fatal("timeout response not as expected")
		}
	}
}

// craft prepares an http.Request and http.ResponseWriter to request timeout of 'dur'.
func craft(dur time.Duration) (*http.Request, *simplerw) {
	return (&http.Request{
			URL: &url.URL{},
			Header: http.Header{
				"Request-Timeout": []string{dur.String()},
			},
		}).WithContext(
			context.WithValue(
				context.Background(),
				"timeout",
				dur,
			),
		), &simplerw{
			hdr: http.Header{},
		}
}

// simplerw is a very simple http.ResponseWriter implementation.
type simplerw struct {
	hdr  http.Header
	code int
	buf  byteutil.Buffer
}

func (rw *simplerw) Header() http.Header {
	return rw.hdr
}

func (rw *simplerw) WriteHeader(status int) {
	rw.code = status
}

func (rw *simplerw) Write(b []byte) (int, error) {
	return rw.buf.Write(b)
}
