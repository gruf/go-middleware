module codeberg.org/gruf/go-middleware

go 1.19

require (
	codeberg.org/gruf/go-byteutil v1.1.2
	codeberg.org/gruf/go-ctx v1.0.2
	codeberg.org/gruf/go-kv v1.6.4
	codeberg.org/gruf/go-ulid v1.1.0
	github.com/felixge/httpsnoop v1.0.3
	golang.org/x/crypto v0.12.0
)
