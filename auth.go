package middleware

import (
	"fmt"
	"net/http"

	"codeberg.org/gruf/go-byteutil"
	"codeberg.org/gruf/go-ulid"
	"golang.org/x/crypto/bcrypt"
)

// Authenticator is a simple interface to provide HTTP basic authentication
// of a given username and password. This could support single or multiple users.
type Authenticator interface {
	Authenticate(username, password string) bool
}

// BasicAuth adds HTTP basic authentication to the given handler, passing each user / pass
// provided in the HTTP authorization header to the given Authenticator{} for authentication.
func (m *Middleware) BasicAuth(handler http.Handler, auth Authenticator) http.Handler {
	// Generate unique realm name.
	realm := ulid.MustNew().String()

	// Generate auth header value.
	authRealm := fmt.Sprintf(
		`Basic realm="%s", charset="UTF-8"`,
		realm,
	)

	return http.HandlerFunc(func(rw http.ResponseWriter, r *http.Request) {
		// Ensure auth header is set indicating required auth.
		rw.Header().Set("WWW-Authenticate", authRealm)

		// Check for provided basic auth.
		user, pass, ok := r.BasicAuth()
		if !ok {
			http.Error(rw, "Unauthorized", http.StatusUnauthorized)
			return
		}

		// Authenticate provided user and pass.
		if !auth.Authenticate(user, pass) {
			http.Error(rw, "Unauthorized", http.StatusUnauthorized)
			return
		}

		// Pass on to given handler.
		handler.ServeHTTP(rw, r)
	})
}

// SingleUserBcrypt returns an Authenticator for a single given user credentials with bcrypt hashed password.
func SingleUserBcrypt(username string, passhash []byte) Authenticator {
	return &bcryptauth{
		user: username,
		hash: passhash,
	}
}

// SingleUserBcryptUnsafe returns the same as SingleUserBcrypt but hashing the given password string beforehand,
// this is unsafe as depending on the means of input to the app you should not be passing in a plain-text password.
func SingleUserBcryptUnsafe(username, password string) Authenticator {
	return SingleUserBcrypt(username, func() []byte {
		hash, err := bcrypt.GenerateFromPassword(
			[]byte(password),
			bcrypt.DefaultCost,
		)
		if err != nil {
			panic(err)
		}
		return hash
	}())
}

type bcryptauth struct {
	user string
	hash []byte
}

func (auth *bcryptauth) Authenticate(username, password string) bool {
	// Check for correct user.
	if username != auth.user {
		return false
	}

	// Compare pre-hashed password and input.
	if err := bcrypt.CompareHashAndPassword(
		auth.hash,
		byteutil.S2B(password),
	); err != nil {

		if err != bcrypt.ErrMismatchedHashAndPassword {
			panic(err) // likely bad provided hash.
		}

		// Incorrect.
		return false
	}

	return true
}
